// import Image from "next/image";
// import Sidebar from "@/components/sidebar";
import BottomNav from "@/components/bottomnav";
import Nav from "@/components/nav";

export default function Cart() {
  return (
    <>
      <Nav />
      <div className="hero min-h-screen bg-base-200" style={{
    backgroundImage: "url(https://images.pexels.com/photos/6766308/pexels-photo-6766308.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1)"
  }}>
        {/* <Sidebar/> */}
        {/* The button to open modal */}
        <label htmlFor="my_modal_6" className="btn">
          open modal
        </label>

        {/* Put this part before </body> tag */}
        <input type="checkbox" id="my_modal_6" className="modal-toggle" />
        <div className="modal">
          <div className="modal-box">
            <h3 className="font-bold text-lg">Hello!</h3>
            <p className="py-4">This modal works with a hidden checkbox!</p>
            <div className="modal-action">
              <label htmlFor="my_modal_6" className="btn">
                Close!
              </label>
            </div>
          </div>
        </div>
        <BottomNav />
      </div>
    </>
  );
}
