// import Image from "next/image";
// import Sidebar from "@/components/sidebar";
import BottomNav from "@/components/bottomnav";
import Nav from "@/components/nav";
import Link from "next/link";

export default function Home() {
  return (
    <>
    <Nav/>
    <div data-theme="retro" className="hero min-h-screen bg-base-200" style={{
    backgroundImage: "url(https://images.pexels.com/photos/3360545/pexels-photo-3360545.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1)"
  }}>
      {/* <Sidebar/> */}
      <div className="hero-content flex-col lg:flex-row-reverse">
      <Link href={"/login"}> <button className="btn btn-info btn-wide">Login to view content</button></Link>
  </div>
      <BottomNav/>
    </div>
    </>
  );
}
