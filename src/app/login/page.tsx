// import Image from "next/image";
// import Sidebar from "@/components/sidebar";
import BottomNav from "@/components/bottomnav";
import Nav from "@/components/nav";

export default function Home() {
  return (
    <>
    <Nav/>
    <div className="hero min-h-screen bg-base-200"
  style={{
    backgroundImage: "url(https://images.pexels.com/photos/298863/pexels-photo-298863.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1)"
  }}>
      {/* <Sidebar/> */}
      <div className="hero-content flex-col lg:flex-row-reverse">
    <div className="text-center lg:text-left">
      <h1 className="text-purple-500 text-5xl font-bold">Login now!</h1>
      <p className="text-black py-6">Provident cupiditate voluptatem et in. Quaerat fugiat ut assumenda excepturi exercitationem quasi. In deleniti eaque aut repudiandae et a id nisi.</p>
    </div>
    <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
      <div className="card-body">
        <div className="form-control">
          <label className="label">
            <span className="label-text">Email</span>
          </label>
          <input type="text" placeholder="email" className="input input-bordered" />
        </div>
        <div className="form-control">
          <label className="label">
            <span className="label-text">Password</span>
          </label>
          <input type="text" placeholder="password" className="input input-bordered" />
          <label className="label">
            <a href="#" className="label-text-alt link link-hover">Forgot password?</a>
          </label>
        </div>
        <div className="form-control mt-6">
          <button className="btn btn-primary">Login</button>
        </div>
      </div>
    </div>
  </div>
      <BottomNav/>
    </div>
    </>
  );
}
