// import Image from "next/image";
// import Sidebar from "@/components/sidebar";
import BottomNav from "@/components/bottomnav";
import Nav from "@/components/nav";

export default function Search() {
  return (
    <>
      <Nav />
      <div data-theme="retro" className="hero min-h-screen bg-base-200">
        {/* <Sidebar/> */}
        <button className="btn">
          <span className="loading loading-spinner"></span>
          loading
        </button>
        <BottomNav />
      </div>
    </>
  );
}
